// Copyright Epic Games, Inc. All Rights Reserved.

#include "TheSnake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TheSnake, "TheSnake" );
