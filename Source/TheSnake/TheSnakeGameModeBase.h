// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THESNAKE_API ATheSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
