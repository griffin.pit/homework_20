// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THESNAKE_TheSnakeGameModeBase_generated_h
#error "TheSnakeGameModeBase.generated.h already included, missing '#pragma once' in TheSnakeGameModeBase.h"
#endif
#define THESNAKE_TheSnakeGameModeBase_generated_h

#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_SPARSE_DATA
#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_RPC_WRAPPERS
#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATheSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ATheSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATheSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TheSnake"), NO_API) \
	DECLARE_SERIALIZER(ATheSnakeGameModeBase)


#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATheSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ATheSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATheSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TheSnake"), NO_API) \
	DECLARE_SERIALIZER(ATheSnakeGameModeBase)


#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATheSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATheSnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATheSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATheSnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATheSnakeGameModeBase(ATheSnakeGameModeBase&&); \
	NO_API ATheSnakeGameModeBase(const ATheSnakeGameModeBase&); \
public:


#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATheSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATheSnakeGameModeBase(ATheSnakeGameModeBase&&); \
	NO_API ATheSnakeGameModeBase(const ATheSnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATheSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATheSnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATheSnakeGameModeBase)


#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_12_PROLOG
#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_SPARSE_DATA \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_RPC_WRAPPERS \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_INCLASS \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_SPARSE_DATA \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TheSnake_Source_TheSnake_TheSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> THESNAKE_API UClass* StaticClass<class ATheSnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TheSnake_Source_TheSnake_TheSnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
