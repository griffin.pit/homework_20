// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THESNAKE_Snake_generated_h
#error "Snake.generated.h already included, missing '#pragma once' in Snake.h"
#endif
#define THESNAKE_Snake_generated_h

#define TheSnake_Source_TheSnake_Snake_h_26_SPARSE_DATA
#define TheSnake_Source_TheSnake_Snake_h_26_RPC_WRAPPERS
#define TheSnake_Source_TheSnake_Snake_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define TheSnake_Source_TheSnake_Snake_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TheSnake"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define TheSnake_Source_TheSnake_Snake_h_26_INCLASS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TheSnake"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define TheSnake_Source_TheSnake_Snake_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public:


#define TheSnake_Source_TheSnake_Snake_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnake)


#define TheSnake_Source_TheSnake_Snake_h_26_PRIVATE_PROPERTY_OFFSET
#define TheSnake_Source_TheSnake_Snake_h_23_PROLOG
#define TheSnake_Source_TheSnake_Snake_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TheSnake_Source_TheSnake_Snake_h_26_PRIVATE_PROPERTY_OFFSET \
	TheSnake_Source_TheSnake_Snake_h_26_SPARSE_DATA \
	TheSnake_Source_TheSnake_Snake_h_26_RPC_WRAPPERS \
	TheSnake_Source_TheSnake_Snake_h_26_INCLASS \
	TheSnake_Source_TheSnake_Snake_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TheSnake_Source_TheSnake_Snake_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TheSnake_Source_TheSnake_Snake_h_26_PRIVATE_PROPERTY_OFFSET \
	TheSnake_Source_TheSnake_Snake_h_26_SPARSE_DATA \
	TheSnake_Source_TheSnake_Snake_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	TheSnake_Source_TheSnake_Snake_h_26_INCLASS_NO_PURE_DECLS \
	TheSnake_Source_TheSnake_Snake_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> THESNAKE_API UClass* StaticClass<class ASnake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TheSnake_Source_TheSnake_Snake_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EmovementDirection::UP) \
	op(EmovementDirection::DOWN) \
	op(EmovementDirection::LEFT) \
	op(EmovementDirection::RIGHT) 

enum class EmovementDirection;
template<> THESNAKE_API UEnum* StaticEnum<EmovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
